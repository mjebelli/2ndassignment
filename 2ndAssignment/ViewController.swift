//
//  ViewController.swift
//  2ndAssignment
//
//  Created by moha on 10/28/1396 AP.
//  Copyright © 1396 moha. All rights reserved.
//
import UIKit

class ViewController: UIViewController {
    
    var sv = UIScrollView();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sv = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        sv.backgroundColor = .white
        sv.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin]
        self.view.addSubview(sv)

        
        
        sv.contentSize = CGSize(width: self.view.frame.size.width * 3, height: self.view.frame.size.height * 3)
        
        
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view.backgroundColor = .red
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight , .flexibleRightMargin, .flexibleBottomMargin]
        sv.addSubview(view)

        let view1 = UIView(frame: CGRect(x: self.view.frame.size.width  , y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view1.backgroundColor = .orange
        view1.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin ,.flexibleBottomMargin]
        sv.addSubview(view1)

        let view2 = UIView(frame: CGRect(x: self.view.frame.size.width * 2 , y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view2.backgroundColor = .yellow
        view2.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin ,.flexibleBottomMargin]
        sv.addSubview(view2)
        
        let view3 = UIView(frame: CGRect(x: 0 , y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view3.backgroundColor = .green
        view3.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleRightMargin, .flexibleTopMargin ,.flexibleBottomMargin]
        sv.addSubview(view3)
        
        
        let view4 = UIView(frame: CGRect(x: self.view.frame.size.width , y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view4.backgroundColor = .brown
        view4.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleRightMargin, .flexibleTopMargin ,.flexibleBottomMargin, .flexibleLeftMargin]
        sv.addSubview(view4)
        
        
        let view5 = UIView(frame: CGRect(x: self.view.frame.size.width * 2 , y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view5.backgroundColor = .gray
        view5.autoresizingMask = [.flexibleWidth, .flexibleHeight,.flexibleRightMargin, .flexibleTopMargin ,.flexibleBottomMargin, .flexibleLeftMargin]
        sv.addSubview(view5)
        
        
        let view6 = UIView(frame: CGRect(x: 0 , y: self.view.frame.size.height * 2 , width: self.view.frame.size.width, height: self.view.frame.size.height))
        view6.backgroundColor = .magenta
        view6.autoresizingMask = [.flexibleWidth, .flexibleHeight,.flexibleRightMargin, .flexibleTopMargin ,.flexibleBottomMargin]
        sv.addSubview(view6)
        
        let view7 = UIView(frame: CGRect(x: self.view.frame.size.width , y: self.view.frame.size.height * 2 , width: self.view.frame.size.width, height: self.view.frame.size.height))
        view7.backgroundColor = .cyan
        view7.autoresizingMask = [.flexibleWidth, .flexibleHeight,.flexibleRightMargin, .flexibleTopMargin ,.flexibleBottomMargin, .flexibleLeftMargin]
        sv.addSubview(view7)
        
        let view8 = UIView(frame: CGRect(x: self.view.frame.size.width * 2 , y: self.view.frame.size.height * 2 , width: self.view.frame.size.width, height: self.view.frame.size.height))
        view8.backgroundColor = .purple
        view8.autoresizingMask = [.flexibleWidth, .flexibleHeight,.flexibleRightMargin, .flexibleTopMargin ,.flexibleBottomMargin, .flexibleLeftMargin]
        sv.addSubview(view8)
        
        sv.isPagingEnabled = true;

    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
       sv.contentSize = CGSize(width: size.width * 3, height: size.height * 3)
        
    }
   
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

